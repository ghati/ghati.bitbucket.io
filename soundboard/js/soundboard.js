/**
 * #Bakchodi Soundboard v3
 * (C) 2002 - All rights are fucking reserved.
 */

console.log("Namaskar Mandali 🙏");

halfmoon.toggleDarkMode();

const RESOURCES_PATH = "/soundboard/resources";

function buildSoundListingUI() {
  fetch(RESOURCES_PATH + "/listing.json", {
    method: "GET",
    redirect: "follow",
  })
    .then((r) => r.json())
    .then((response) => {
      var listingContainer = document.getElementById("lisiting-container");

      // Update Status Bar
      updateNavbarStatus(response);

      for (var i = 0; i < response.length; i++) {
        var groupDetails = response[i];

        var groupDiv = document.createElement("div");
        groupDiv.className = "card";

        var groupDivTitle = document.createElement("h2");
        groupDivTitle.className = "card-title";
        groupDivTitle.textContent = groupDetails["group_name"];

        groupDiv.appendChild(groupDivTitle);

        var soundButtons = buildSoundButtons(groupDetails);
        groupDiv.appendChild(soundButtons);

        listingContainer.appendChild(groupDiv);
      }
    })
    .catch((error) =>
      console.log("Caught error when fetching listing.json!", error)
    );
}

function updateNavbarStatus(response) {
  var numberOfSounds = 0;
  var numberOfCategories = 0;

  for (var i = 0; i < response.length; i++) {
    numberOfCategories++;
    numberOfSounds = numberOfSounds + response[i]["files"].length;
  }

  document.getElementById(
    "navbar-status"
  ).innerText = `Serving ${numberOfSounds} sounds in ${numberOfCategories} categories`;
}

function buildSoundButtons(groupDetails) {
  var element = document.createElement("div");
  var files = groupDetails["files"];

  for (var i = 0; i < files.length; i++) {
    var file = files[i];

    var btn = document.createElement("button");
    btn.className = "btn";
    btn.type = "button";
    btn.textContent = file;
    btn.id = file;
    btn.addEventListener("click", playSound);

    element.appendChild(btn);
  }

  return element;
}

var PLAYING_SOUND;

function playSound(event) {
  var fileLink = RESOURCES_PATH + "/sounds/" + event.target.id + ".mp3";

  if (PLAYING_SOUND == null) {
    PLAYING_SOUND = new Howl({
      src: [fileLink],
    });
  }

  if (PLAYING_SOUND.playing()) {
    PLAYING_SOUND.stop();
  } else {
    PLAYING_SOUND = new Howl({
      src: [fileLink],
    });

    event.target.style.backgroundColor = "#ff9800";
    PLAYING_SOUND.play();
  }

  PLAYING_SOUND.on("stop", function () {
    event.target.style.backgroundColor = "#25292C";
  });

  PLAYING_SOUND.on("end", function () {
    event.target.style.backgroundColor = "#25292C";
  });
}

window.addEventListener("DOMContentLoaded", (event) => {
  buildSoundListingUI();
});
